
let slides = document.querySelectorAll('.slide');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide, 3000);
let pauseButton = document.getElementById('pause');
let startButton = document.getElementsByClassName('start')[0];
console.log(startButton)

function nextSlide() {
  slides[currentSlide].className = 'slide';
  currentSlide = (currentSlide + 1) % slides.length;
  slides[currentSlide].className = 'slide showing';
}

function pauseSlideshow() {
  pauseButton.innerHTML = 'Припинити';
  clearInterval(slideInterval);
  pauseButton.style.display = "none";
  startButton.style.display = "block";
}


pauseButton.onclick = function () {
  pauseSlideshow();

}

function playSlideshow() {
  startButton.innerHTML = 'Відновити показ';
  slideInterval = setInterval(nextSlide, 3000);
}

startButton.onclick = function () {
  slideInterval = setInterval(nextSlide, 3000);
  startButton.style.display = "none";
  pauseButton.style.display = "block";
}
